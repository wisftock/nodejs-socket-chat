const socket = io();

let params = new URLSearchParams(window.location.search);

if (!params.has('nombre') || !params.has('sala')) {
  window.location = 'index.html';
  throw new Error('El nombre y sala son necesarios');
}

let usuario = {
  nombre: params.get('nombre'),
  sala: params.get('sala'),
};

socket.on('connect', function () {
  // console.log('conectado al servidor');
  socket.emit('entrarChat', usuario, function (resp) {
    console.log('Usuario conectado: ', resp);
  });
});

// desconexion del usuario o servidor
socket.on('disconnect', function () {
  console.log('perdimos conexion con el servidor');
});

// enviar informacion
// socket.emit(
//   'crearMensaje',
//   {
//     user: 'pepe',
//     message: 'Hola mundo',
//   },
//   function (respt) {
//     console.log('respuesta server: ', respt);
//   }
// );

// escuchar informacion
socket.on('crearMensaje', function (message) {
  console.log('Servidor :', message);
});

// escuchar cambios de usuarios cuando un usuario entra o sale del chat
socket.on('listaPersona', function (persona) {
  console.log(persona);
});

// mensajes privados
socket.on('mensajePrivado', function (mensaje) {
  console.log('Mensaje privado: ', mensaje);
});
