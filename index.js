const express = require('express');
const socketIO = require('socket.io');
const http = require('http');

const path = require('path');

const app = express();

// creando servidor
const server = http.createServer(app);

const port = process.env.PORT || 3000;
const publicPath = path.resolve(__dirname, './public');
app.use(express.static(publicPath));

// inicializar socket io / esta la comunicacion con el backend
module.exports.io = socketIO(server);
require('./sockets/sockets');

server.listen(port, () => {
  console.log(`http://localhost:${port}`);
});
