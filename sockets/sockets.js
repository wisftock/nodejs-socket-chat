const { io } = require('../index');
const { Usuarios } = require('../classes/usuario');
const { crearMensaje } = require('../utils/utilidades');
const usuarios = new Usuarios();

io.on('connection', (client) => {
  client.on('entrarChat', (data, callback) => {
    if (!data.nombre || !data.sala) {
      return callback({
        message: 'El nombre/sala es necesario',
      });
    }

    client.join(data.sala);

    usuarios.agregarPersona(client.id, data.nombre, data.sala);

    client.broadcast
      .to(data.sala)
      .emit('listaPersona', usuarios.getPersonaPorSala(data.sala));

    callback(usuarios.getPersonaPorSala(data.sala));
  });

  client.on('crearMensaje', (data) => {
    let persona = usuarios.getPersona(client.id);
    let mensaje = crearMensaje(persona.nombre, data.mensaje);
    client.broadcast.to(persona.sala).emit('crearMensaje', mensaje);
  });

  client.on('disconnect', () => {
    let personaBorrada = usuarios.getPersonaBorrar(client.id);

    client.broadcast
      .to(personaBorrada.sala)
      .emit(
        'crearMensaje',
        crearMensaje('Administrador', `${personaBorrada.nombre} salio del chat`)
      );

    client.broadcast
      .to(personaBorrada.sala)
      .emit('listaPersona', usuarios.getPersonaPorSala(personaBorrada.sala));
  });

  // mensaje privado
  client.on('mensajePrivado', (data) => {
    let persona = usuarios.getPersona(client.id);
    client.broadcast
      .to(data.para)
      .emit('mensajePrivado', crearMensaje(persona.nombre, data.mensaje));
  });
});
